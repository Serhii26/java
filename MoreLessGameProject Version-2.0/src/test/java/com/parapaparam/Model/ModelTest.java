package com.parapaparam.Model;

import com.parapaparam.Attempt;
import com.parapaparam.Controller.Controller;
import com.parapaparam.SecretValue;
import com.parapaparam.View.View;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class ModelTest {

    Model model;
    Integer expectedValue;

    @Before
    private void setUp() {
        model = new Model();
    }

    @Test
    public void TestNumberOnLowerBarrier() throws Exception {
        expectedValue = 1;
        for (int i = 0; i < 10000; i++) {
            model.rand();
            if (expectedValue == model.getSecretValue()) {
                return;
            }
        }
        Assert.fail();
    }

    @Test
    public void TestNumberLessLowerBarrier() throws Exception {
        expectedValue = 0;
        for (int i = 0; i < 10000; i++) {
            model.rand();
            if (expectedValue == model.getSecretValue()) {
                Assert.fail();
            }
        }
        return;
    }

    @Test
    public void TestNumberOnUpperBarrier() throws Exception {
        expectedValue = 99;
        for (int i = 0; i < 100000; i++) {
            model.rand();
            if (expectedValue == model.getSecretValue()) {
                return;
            }
        }
        Assert.fail();
    }

    @Test
    public void TestNumberOverUpperBarrier() throws Exception {
        expectedValue = 100;
        for (int i = 0; i < 100000; i++) {
            model.rand();
            if (expectedValue == model.getSecretValue()) {
                Assert.fail();
            }
        }
        return;
    }

    @Test
    public void TestAllValuesInRange() throws Exception {
        int count = 0;
        int capacity = model.getMaxValue() - 1;
        int secretValue;

        HashMap<Integer, Boolean> values = new HashMap<>(capacity);

        for (int i = 1; i <= capacity; i++) {
            values.put(i, false);
        }

        for (int i = 0; i < 1000; i++) {
            model.rand();
            secretValue = model.getSecretValue();
            if (!values.get(secretValue)) {
                values.put(secretValue, true);
                count++;
            }
        }

        if (count != capacity) {
            Assert.fail("Rand returnes not all values in range");
        }
        return;
    }

    @Test
    public void TestEnum() throws Exception {
        View view = new View();
        Controller controller = new Controller(model, view);

        model.setSecretValue(SecretValue.SIXTH);
        controller.inputDataWithEnum(Attempt.FIFTH);
//        for (Attempt val : Attempt.values()) {
//            controller.inputDataWithEnum(val);
//        }
    }


}