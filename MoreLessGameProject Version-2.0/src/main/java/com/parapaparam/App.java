package com.parapaparam;

/**
 * Hello world!
 */

class First {
    int value = 3;

    void addFive() {
        value += 5;
        System.out.println("first ");
    }
}

class Second extends First{
    int value = 2;

    public int getValue() {
        return value;
    }

    void addFive() {
        value += 5;
        System.out.println("second ");
    }
}

public class App {
    public static void main(String[] args) {
        First f =new Second();
        f.addFive();
    }
}
