package com.parapaparam.controller;

import com.parapaparam.model.entities.Pion;
import com.parapaparam.model.entities.enums.Condition;
import com.parapaparam.model.services.PionService;

public class ConsoleControler {

	public static void main(String[] args) {
		PionService pionService = new PionService();

		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}

		System.out.println("Get Pions by Id " + pionService.getPionById(3).get());

		System.out.println("Add Pion " + pionService.insertPion(new Pion(Condition.GOOD, 9)));
		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}
		System.out.println("Delete Pion by Id " + pionService.deletePionById(44));
		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}

		System.out.println("Add Pion " + pionService.insertPion(new Pion(Condition.GOOD, 7)));
		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}
		System.out.println("Delete Pion by Id " + pionService.deletePion(pionService.getPionById(53).get()));
		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}

		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}

		System.out.println("Add Pion " + pionService.insertPion(new Pion(Condition.GOOD, 4)));

		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}

		System.out.println("Update Pion " + pionService.updatePion(new Pion(Condition.BAD, 5)));

		System.out.println("Get All Pions");
		for (Pion pion : pionService.getAllPions()) {
			System.out.println(pion);
		}
	}
}
