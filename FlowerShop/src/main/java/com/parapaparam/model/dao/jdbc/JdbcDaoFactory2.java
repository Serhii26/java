package com.parapaparam.model.dao.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.cj.jdbc.Driver;
import com.parapaparam.model.dao.DaoConnection;
import com.parapaparam.model.dao.DaoFactory;
import com.parapaparam.model.dao.PionDao;
import com.parapaparam.model.dao.RoseDao;

public class JdbcDaoFactory2 extends DaoFactory {

	private Connection connection;
	private static final String DB_URL = "url";

	public JdbcDaoFactory2() {
		createConnection();
	}

	@Override
	public DaoConnection getConnection() {
		try {
			if (connection.isClosed()) {
				createConnection();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return new JdbcDaoConnection(connection);
	}

	private void createConnection() {
		try {
			InputStream inputStream = DaoFactory.class.getResourceAsStream(DB_FILE);
			Properties dbProps = new Properties();
			dbProps.load(inputStream);
			String url = dbProps.getProperty(DB_URL);
			new Driver();
			connection = DriverManager.getConnection(url, dbProps);
		} catch (SQLException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public PionDao createPionDao() {
		return null;
	}

	@Override
	public PionDao createPionDao(DaoConnection connection) {
		JdbcDaoConnection jdbcConnection = (JdbcDaoConnection) connection;
		Connection sqlConnection = jdbcConnection.getConnection();
		return new JdbcPionDao(sqlConnection);
	}

	@Override
	public RoseDao createRoseDao() {
		return null;
	}

	@Override
	public RoseDao createRoseDao(DaoConnection connection) {
		JdbcDaoConnection jdbcConnection = (JdbcDaoConnection) connection;
		Connection sqlConnection = jdbcConnection.getConnection();
		return new JdbcRoseDao(sqlConnection);
	}
	
	

}
