package com.parapaparam.model.entities.enums;

public enum Condition {

	BAD("bad"), GOOD("good");

	private final String value;

	Condition(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
