package com.parapaparam.controller.commands;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.parapaparam.model.entities.Pion;
import com.parapaparam.model.entities.Rose;
import com.parapaparam.model.services.PionService;
import com.parapaparam.model.services.RoseService;

public class GetRoses implements Command {

	RoseService roseService = new RoseService();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Rose> allRoses = roseService.getAllRoses();

		request.setAttribute("roses", allRoses);

		return "/Roses.jsp";
	}

}
