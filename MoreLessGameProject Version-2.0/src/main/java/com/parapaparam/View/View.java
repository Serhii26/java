package com.parapaparam.View;

import java.util.ArrayList;

/**
 * Created by Serhii_Bondarchuk on 4/18/2017.
 */
public class View {

    // Text's constants
    public static final String INTRO = "Welcome to the game more, less!";
    public static final String MORE = "Your value is more!";
    public static final String LESS = "Your value is less!";
    public static final String BYE = "Good Bye!";
    public static final String WRONG_INPUT = "WRONG INPUT!";
    public static final String CONGRATULATION = "CONGRATULATION! YOU WIN!";

    public static final String IMITATIONINPUT = "50\n25\n75\n30\n15\n66\n88\n44\nq";

    public void printAll(Integer minValue, Integer maxValue, ArrayList<Integer> hist) {
        printHistory(hist);
        printRules(minValue, maxValue);
//        System.out.println("Last attempt: " + hist.get(hist.size() - 1));
    }

    public void printRules(Integer minValue, Integer maxValue) {
        System.out.println("Input the value in range from " + minValue + " exclusive to " + maxValue + " exclusive!");
    }

    public void print(String message) {
        System.out.println(message);
    }

    public void printHistory(ArrayList<Integer> hist) {
        for (int i = 0; i < hist.size(); i++) {
            System.out.println((i + 1) + " attempt: " + hist.get(i));
        }
    }

    public void printCongrat(ArrayList<Integer> statistics, Integer secretValue) {
        System.out.println(CONGRATULATION);
        System.out.println("Secret Value is " + secretValue);
        printHistory(statistics);
    }

    public void printWrong(Integer minValue, Integer maxValue) {
        System.out.println(WRONG_INPUT);
        printRules(minValue, maxValue);
    }
}
