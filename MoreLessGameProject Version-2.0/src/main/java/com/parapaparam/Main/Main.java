package com.parapaparam.Main;

import com.parapaparam.Controller.Controller;
import com.parapaparam.Model.Model;
import com.parapaparam.View.View;

/**
 * Created by Serhii_Bondarchuk on 4/18/2017.
 */
public class Main {

    public static void main(String[] args) {
        // Initialisation
        View view = new View();
        Model model = new Model();
        Controller controller = new Controller(model, view);

        // Run
        controller.processUser();

    }
}
