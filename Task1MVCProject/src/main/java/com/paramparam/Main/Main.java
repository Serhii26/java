package com.paramparam.Main;

import com.paramparam.Controller.Controller;
import com.paramparam.Model.Model;
import com.paramparam.View.View;

/**
 * Created by Serhii_Bondarchuk on 4/13/2017.
 */
public class Main {

    public static void main(String[] args) {

        // Initialisation
        View view = new View();
        Model model = new Model();

        Controller controller = new Controller(model, view);
        // Run
        controller.processUser();
    }
}
