package com.paramparam.View;

import java.util.Arrays;

/**
 * Created by Serhii_Bondarchuk on 4/13/2017.
 */
public class View {

    // Text's constants
    public static final String INPUT_ARRAY_DATA = "Input elements of massive! To exit input any letter!";
    public static final String INPUT_VALUE_DATA = "Input value!";
    public static final String WRONG_INPUT_WORD_DATA = "Wrong input! Repeat please! ";

    public static final String GET_SUM_MESSAGE = "Sum of array: ";
    public static final String GET_MAX_MESSAGE = "Max element of array:  ";
    public static final String GET_AVERAGE_MESSAGE = "Average of array elements: ";
    public static final String GET_ARRAY_MULT_BY_NUMBER_MESSAGE = "Array multiply by number: ";
    public static final String GET_CYCLIC_SHIFT_MESSAGE = "Array with cyclic shift: ";
    public static final String GET_MODIFIED_ARRAY_MESSAGE = "Modified array: ";

    public static void print(String message){
        System.out.println(message);
    }

    public static void print(Integer[] array){
        System.out.println(Arrays.toString(array));
    }

    public static void print(Integer value){
        System.out.println(value);
    }
}
