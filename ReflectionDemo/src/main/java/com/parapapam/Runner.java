package com.parapapam;

import com.parapapam.Resources.CustomAnnotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Runner {

    public static void main(String[] args) {
        Class clazz;
        Object entity = null;
        try {
            clazz = Class.forName("com.parapapam.Model.QueueX");
            entity = clazz.getDeclaredConstructor(int.class).newInstance(3);

            clazz.getMethod("push", long.class).invoke(entity, 33);

            getMethodsData(clazz, entity);

    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("Superclass Name: " + entity.getClass().getSuperclass());

    }

    static void getMethodsData(Class cl, Object en) throws InvocationTargetException, IllegalAccessException {
        for (Method method : cl.getDeclaredMethods()) {
            System.out.print("Method: " + method.getName());
            System.out.print("\t Modifiers: " + getModifiersName(method.getModifiers()));
            System.out.println("\t Type: " + method.getTypeParameters());
            if (method.isAnnotationPresent(CustomAnnotation.class)) {
                Long result;
                result = (Long) method.invoke(en);
                System.out.println(result);
            }
        }
    }

    static String getModifiersName(int mods) {
        if (Modifier.isPublic(mods)) {
            return "public";
        }
        if (Modifier.isAbstract(mods)) {
            return "abstract";
        }
        if (Modifier.isFinal(mods)) {
            return "final";
        }
        if (Modifier.isPrivate(mods)) {
            return "private";
        }
        if (Modifier.isStatic(mods)) {
            return "static";
        }
        return "Unknown modifier";
    }
}
