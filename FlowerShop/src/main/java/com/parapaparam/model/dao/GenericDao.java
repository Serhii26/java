package com.parapaparam.model.dao;

import java.util.List;
import java.util.Optional;

import com.parapaparam.model.entities.Flower;

public interface GenericDao<T extends Flower> {

	public Optional<T> getById(Integer id);

	public List<T> getAll();
	public Boolean add(T flower);
	public Boolean update(T flower);
	public Boolean delete(T flower);
	public Boolean delete(Integer id);

}
