package com.parapaparam.Controller;

import com.parapaparam.Model.Model;
import com.parapaparam.View.View;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.mockito.Mockito.*;

public class ControllerMockitoTest {

    Model model;
    View view;
    Controller controller;

    @Before
    public void setUp() {
        model = mock(Model.class);
        view = mock(View.class);

        System.setIn(new ByteArrayInputStream(view.IMITATIONINPUT.getBytes()));

        doNothing().when(model).rand();
    }

    @Test
    public void TestControllerWithMockModelAndView() {
        controller = new Controller(model, view);
        controller.processUser();

        verify(model, atLeastOnce()).rand();
    }

    @Test
    public void TestControllerWithSpyModelAndView() {
        controller = new Controller(new Model(), new View());

        controller.processUser();
    }
}
