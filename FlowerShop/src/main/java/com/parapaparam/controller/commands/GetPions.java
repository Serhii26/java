package com.parapaparam.controller.commands;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.parapaparam.model.entities.Pion;
import com.parapaparam.model.services.PionService;

public class GetPions implements Command {

	PionService pionService = new PionService();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Pion> allPions = pionService.getAllPions();

		request.setAttribute("pions", allPions);

		return "/Pions.jsp";
	}

}
