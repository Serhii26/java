package com.epam;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - i; j++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }
    }
}
