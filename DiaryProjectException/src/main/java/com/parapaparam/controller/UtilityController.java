package com.parapaparam.controller;

import com.parapaparam.view.View;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Serhii_Bondarchuk on 4/20/2017.
 */
public class UtilityController {

    String inputSurnameWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.SURNAME);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.name)))) {
            view.print(view.WRONG);
            view.print(view.SURNAME);
//            input.setLength(0);
            input.delete(0, input.length());
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputNameWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.NAME);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.name)))) {
            view.print(view.WRONG);
            view.print(view.NAME);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputPatronymicWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.PATRONYMIC);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.patronymic)))) {
            view.print(view.WRONG);
            view.print(view.PATRONYMIC);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputNicknameWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.NICKNAME);
        while ( !((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.nickname))) ) {
            view.print(view.WRONG);
            view.print(view.NICKNAME);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputCommentWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.COMMENT);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.comment)))) {
            view.print(view.WRONG);
            view.print(view.COMMENT);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputGroupWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.GROUP);
        if (sc.hasNext()) {
            input.append(sc.next());
            while (!contains(input.toString())) {
                view.print(view.WRONG);
                input.delete(0, input.length());
                view.print(view.GROUP);
                input.append(sc.next());
            }
            view.print(view.CORRECT);
        }

        return input.toString().toUpperCase();
    }

    boolean contains(String input) {
        for (Group group : Group.values()) {
            if (input.equalsIgnoreCase(group.names())) {
                return true;
            }
        }
        return false;
    }

    String inputHomephoneWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.HOMEPHONE);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.homePhone)))) {
            view.print(view.WRONG);
            view.print(view.HOMEPHONE);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputPhoneWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.PHONE);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.phone)))) {
            view.print(view.WRONG);
            view.print(view.PHONE);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputEmailWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.EMAIL);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.email)))) {
            view.print(view.WRONG);
            view.print(view.EMAIL);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputSkypeWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.SKYPE);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.skype)))) {
            view.print(view.WRONG);
            view.print(view.SKYPE);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputIndexWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.INDEX);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.index)))) {
            view.print(view.WRONG);
            view.print(view.INDEX);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputCityWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.CITY);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.city)))) {
            view.print(view.WRONG);
            view.print(view.CITY);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputStreetWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.STREET);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.street)))) {
            view.print(view.WRONG);
            view.print(view.STREET);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputHouseNumberWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.HOUSENUMBER);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.houseNumber)))) {
            view.print(view.WRONG);
            view.print(view.HOUSENUMBER);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

    String inputFlatNumberWithScanner(View view, Scanner sc) {

        StringBuffer input = new StringBuffer();

        view.print(view.FLATNUMBER);
        while (!((sc.hasNext()) && (input.append(sc.next()).toString().matches(RegexpContainer.flatNumber)))) {
            view.print(view.WRONG);
            view.print(view.FLATNUMBER);
            input.setLength(0);
        }
        view.print(view.CORRECT);

        return input.toString();
    }

}
