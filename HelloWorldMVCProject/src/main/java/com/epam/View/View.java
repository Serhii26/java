package com.epam.View;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Seroga on 12.04.2017.
 */
public class View {

    // Text's constants
    public static final String INPUT_FIRST_WORD_DATA = "Input Hello! To quit input exit!";
    public static final String INPUT_SECOND_WORD_DATA = "Input World! To quit input exit!";
    public static final String WRONG_INPUT_WORD_DATA = "Wrong input! Repeat please!";
    public static final String EXIT = "exit";

    public static void printMessage(String message){
        System.out.println(message);
    }

//    public static void printArray(ArrayList<Integer> array){
//        System.out.println(Arrays.toString(array.toArray()));
//    }
}
