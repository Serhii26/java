package com.parapaparam.model;

import java.util.ArrayList;

/**
 * Created by Serhii_Bondarchuk on 4/21/2017.
 */
public class NoteBook {

    ArrayList<Note> records = new ArrayList<Note>();

    public ArrayList<Note> getRecord() {
        return records;
    }

    public void setRecord(ArrayList<Note> records) {
        this.records = records;
    }

    public Note getRecords(int index) {
        return records.get(index);
    }

    public void setRecords(Note inputRecord) throws DuplicateDataException {

        // Imitating situation when Abonent with this nickname already exists.
        for (Note note : records) {
            if (inputRecord.getNickname().equals(note.getNickname())) {
                throw new DuplicateDataException("Abonent with nickname: \"" + inputRecord.getNickname() + "\" already exists");
            }
        }
        records.add(inputRecord);
    }

    @Override
    public String toString() {

        StringBuffer allRecords = new StringBuffer();

        for (Note record : records) {
            allRecords.append(record);
        }
        return allRecords.toString();
    }
}
