package com.parapaparam.controller;

/**
 * Created by Serhii_Bondarchuk on 4/25/2017.
 */
public enum Group {
    AT("AT", "Automated testing"),
    SFT("SFT", "Software Functional Testing"),
    FRONTEND("FRONY-END", "Front End"),
    DOTNET("DOTNET", ".NET"),
    JAVA("JAVA", "Java");

    private final String names;
    private final String description;

    Group(String names, String description) {
        this.names = names;
        this.description = description;
    }

    public String names() {
        return names;
    }

    public String description() {
        return description;
    }

}
