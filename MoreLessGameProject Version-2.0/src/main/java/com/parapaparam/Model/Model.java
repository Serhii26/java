package com.parapaparam.Model;

import com.parapaparam.SecretValue;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Serhii_Bondarchuk on 4/18/2017.
 */
public class Model {

    private Integer minValue = 0;
    private Integer maxValue = 100;

    private Integer secretValue;

    private ArrayList<Integer> log = new ArrayList<>();

    public void setSecretValue(SecretValue value) {
        this.secretValue = value.getValue();
    }

    public Model() {
    }

    public void setLog(Integer attempt) {
        if (log == null) {
            log = new ArrayList<>();
        }
        log.add(attempt);
    }

    public ArrayList<Integer> getLog() {
        return log;
    }

    public Integer getMinValue() {
        return minValue;
    }

    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public Integer getSecretValue() {
        return secretValue;
    }

    public void rand() {
        Random rnd = new Random();
        secretValue = rnd.nextInt(maxValue - minValue - 1) + 1;
    }
}
