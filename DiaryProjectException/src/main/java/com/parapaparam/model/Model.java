package com.parapaparam.model;

import java.io.*;
import java.util.ArrayList;

import static java.lang.System.out;

/**
 * Created by Serhii_Bondarchuk on 4/21/2017.
 */
public class Model {

    private Note note;
    private NoteBook noteBook;

    public Model() {
        note = new Note();
        noteBook = new NoteBook();
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public NoteBook getNoteBook() {
        return noteBook;
    }

//    public void setNoteBook(NoteBook noteBook) {
//        this.noteBook = noteBook;
//    }

    public String createInitials(String surname, String name) {
        StringBuffer initials = new StringBuffer();
        initials.append(surname + " " + name.substring(0, 1) + ".");
        return initials.toString();
    }

    public String createFullAddress(String[] address) {
        StringBuffer fullAdress = new StringBuffer();
        for (String part : address) {
            fullAdress.append(part + ", ");
        }
        return fullAdress.toString();
    }

    public String createFullAddress(ArrayList<String> address) {
        StringBuffer fullAdress = new StringBuffer();
        for (String part : address) {
            fullAdress.append(part + ", ");
        }
        return fullAdress.toString();
    }

    public boolean exportToFile() {
        try (FileWriter out = new FileWriter("Notebook.txt")) {
            for (Note n : noteBook.getRecord()){
                out.write(n.toString());
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
