package com.epam.Controller;

import com.epam.Model.Model;
import com.epam.View.View;

import java.util.Scanner;

/**
 * Created by Seroga on 12.04.2017.
 */
public class Controller {

    Model model;
    View view;

    public static final String REGEX_FIRST_WORD = "hello";
    public static final String REGEX_SECOND_WORD = "world";


    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    // The Work method
    public void processUser() {
        Scanner sc = new Scanner(System.in);

        model.setMessage1(inputMessageWithScanner(sc, view.INPUT_FIRST_WORD_DATA, REGEX_FIRST_WORD));
        model.setMessage2(inputMessageWithScanner(sc, view.INPUT_SECOND_WORD_DATA, REGEX_SECOND_WORD));

        model.setMessage();
        view.printMessage(model.getMessage());
    }

    // The Utility methods
    public String inputMessageWithScanner(Scanner sc, String inputData, String word) {
        String str;
        view.printMessage(inputData);
        while (true) {
            str = sc.next();
            while (!str.equalsIgnoreCase(word)) {
                view.printMessage(view.WRONG_INPUT_WORD_DATA + inputData);
                str = sc.next();
            }
            break;
        }
        return str;
    }

}
