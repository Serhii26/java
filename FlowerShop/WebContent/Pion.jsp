<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="2 solid black">
		<thead>
			<tr>
				<td>Pion ID</td>
				<td>Expiration Date</td>
				<td>Shelf Life</td>
				<td>Length</td>
				<td>Price</td>
				<td>Conditions</td>
				<td>Role</td>
			</tr>
		</thead>
			<tr>
				<td>${pion.id}</td>
				<td>${pion.expirationDate}</td>
				<td>${pion.shelfLife}</td>
				<td>${pion.length}</td>
				<td>${pion.price}</td>
				<td>${pion.condition}</td>
				<td>${pion.role}</td>
			</tr>
	</table>
	<a href="/FlowerShop/index.jsp">Back</a>
</body>
</html>