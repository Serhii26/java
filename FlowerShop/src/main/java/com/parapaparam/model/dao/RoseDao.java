package com.parapaparam.model.dao;

import com.parapaparam.model.entities.Rose;

public interface RoseDao extends GenericDao<Rose> {

}
