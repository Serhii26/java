package com.parapaparam.model.entities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.parapaparam.model.entities.Rose.Builder;
import com.parapaparam.model.entities.enums.Condition;
import com.parapaparam.model.entities.enums.Role;

public class Rose extends Flower {

	public Rose() {
		super();
	}

	public Rose(Condition condition, Integer id) {
		super();
		this.id = id;
		length = 60;
		price = 20.0;
		this.condition = condition;
		role = Role.SECONDARY;
		calcExpirationDate();
	}

	@Override
	void calcExpirationDate() {
		if (condition == Condition.GOOD) {
			shelfLife = 4;
		} else
			shelfLife = 1;
		expirationDate = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm").format(LocalDateTime.now()) + shelfLife;
	}

	@Override
	public String toString() {
		return "Rose{" + " id = " + id + ", length = " + length + ", condition = " + condition + ", shelfLife = "
				+ shelfLife + ", expirationDate = " + expirationDate + ", price = " + price + ", role = " + role + '}';
	}

	public static class Builder {
		Rose instance = new Rose();

		public Builder setId(int id) {
			instance.id = id;
			return this;
		}

		public Builder setExpirationDate(String expirationDate) {
			instance.expirationDate = expirationDate;
			return this;
		}

		public Builder setShelfLife(Integer shelfLife) {
			instance.shelfLife = shelfLife;
			return this;
		}

		public Builder setMaxLength(Integer maxLength) {
			instance.length = maxLength;
			return this;
		}

		public Builder setPrice(Double price) {
			instance.price = price;
			return this;
		}

		public Builder setCondition(Condition condition) {
			instance.condition = condition;
			return this;
		}

		public Builder setRole(Role role) {
			instance.role = role;
			return this;
		}

		/*
		 * public Builder setName(String name , boolean isNull) { if(!isNull) {
		 * instance.name = name; }else{ instance.name = null; } return this; }
		 */

		public Rose build() {
			return instance;
		}

	}
}
