package com.parapaparam.model.entities;

import com.parapaparam.model.entities.enums.Condition;
import com.parapaparam.model.entities.enums.Role;

public abstract class Flower {

	protected Integer id;

	protected String expirationDate;
	protected Integer shelfLife;
	protected Integer length;
	protected Double price;
	protected Condition condition;
	protected Role role;

	abstract void calcExpirationDate();

	public Flower() {
		super();
	}
	
	public Flower(Condition condition) {
		super();
		this.condition = condition;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getShelfLife() {
		return shelfLife;
	}

	public void setShelfLife(Integer shelfLife) {
		this.shelfLife = shelfLife;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
		calcExpirationDate();
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}
