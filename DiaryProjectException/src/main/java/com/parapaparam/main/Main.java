package com.parapaparam.main;

import com.parapaparam.controller.Controller;
import com.parapaparam.model.Model;
import com.parapaparam.view.View;

/**
 * Created by Serhii_Bondarchuk on 4/20/2017.
 */
public class Main {

    public static void main(String[] args) {
        new Controller(new Model(), new View()).processUser();
    }
}
