package com.parapaparam;

/**
 * Created by Serhii_Bondarchuk on 4/19/2017.
 */
public enum Attempt {
    FIRST ("1"),
    SECOND ("2"),
    THIRD ("3"),
    FOURTH ("4"),
    FIFTH ("5"),
    SIXTH ("6"),
    WRONG ("d"),
    QUIT ("q");

    private final String value;

    Attempt(String mass) {
        this.value = mass;
    }
    public String getValue() { return value; }
}
