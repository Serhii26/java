package com.parapaparam.model;

/**
 * Created by Serhii_Bondarchuk on 4/27/2017.
 */
public class DuplicateDataException extends Exception {

    public DuplicateDataException() {
    }

    public DuplicateDataException(String message) {
        super(message);
    }

    public DuplicateDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateDataException(Throwable cause) {
        super(cause);
    }

}
