package com.epam.Main;

import com.epam.Controller.Controller;
import com.epam.Model.Model;
import com.epam.View.View;

public class Main {

    public static void main(String[] args) {
        // Initialisation
        View view = new View();
        Model model = new Model();

        Controller controller = new Controller(model, view);
        // Run
        controller.processUser();
    }

}
