package com.parapaparam.model.entities.enums;

public enum Accessory {
	RIBBON("", 10.0), COLORPAPER("", 20.0), BOW("", 15.0);

	private final String description;
	private final Double price;

	Accessory(String description, Double price) {
		this.description = description;
		this.price = price;
	}

	public Double getPrice() {
		return price;
	}

}
