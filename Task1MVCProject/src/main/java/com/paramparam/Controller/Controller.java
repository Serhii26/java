package com.paramparam.Controller;

import com.paramparam.Model.Model;
import com.paramparam.View.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Serhii_Bondarchuk on 4/13/2017.
 */
public class Controller {

    Model model;
    View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    // The Work method
    public void processUser() {

        // 001
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.getSum();
        view.print(view.GET_SUM_MESSAGE);
        view.print(model.getResultValue());

        // 002
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.getMax();
        view.print(view.GET_MAX_MESSAGE);
        view.print(model.getResultArray()[0]);

        // 004
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.getAverage();
        view.print(view.GET_AVERAGE_MESSAGE);
        view.print(model.getResultValue());

        // 008
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.setInputValue(inputValueWithScanner(view.INPUT_VALUE_DATA));
        model.getArrayMultByNumber();
        view.print(view.GET_ARRAY_MULT_BY_NUMBER_MESSAGE);
        view.print(model.getResultArray());

        // 018
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.setInputValue(inputValueWithScanner(view.INPUT_VALUE_DATA));
        model.getCyclicShift();
        view.print(view.GET_CYCLIC_SHIFT_MESSAGE);
        view.print(model.getResultArray());

        // 025
        model.setInputArray(inputArrayWithScanner(view.INPUT_ARRAY_DATA));
        model.getExchangeFirstAndLast();
        view.print(view.GET_MODIFIED_ARRAY_MESSAGE);
        view.print(model.getResultArray());
    }

    // The Utility methods
    public Integer[] inputArrayWithScanner(String inputData) {

        Scanner sc = new Scanner(System.in);

        ArrayList<Integer> list = new ArrayList<Integer>();

        view.print(inputData);

        while (sc.hasNextInt()) {
            list.add(sc.nextInt());
        }

        Integer[] array = new Integer[list.size()];
        array = list.toArray(array);
//        System.out.println(Arrays.toString(list.toArray()));

        return array;
    }

    // The Utility methods
    public Integer inputValueWithScanner(String inputData) {

        Scanner sc = new Scanner(System.in);

        view.print(inputData);

        while (!sc.hasNextInt()) {
            view.print(view.WRONG_INPUT_WORD_DATA);
        }

        return sc.nextInt();
    }
}
