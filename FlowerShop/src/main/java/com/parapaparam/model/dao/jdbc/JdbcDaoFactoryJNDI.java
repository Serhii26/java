package com.parapaparam.model.dao.jdbc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.mysql.cj.jdbc.Driver;
import com.parapaparam.model.dao.DaoConnection;
import com.parapaparam.model.dao.DaoFactory;
import com.parapaparam.model.dao.PionDao;
import com.parapaparam.model.dao.RoseDao;

public class JdbcDaoFactoryJNDI extends DaoFactory {

	private DataSource dataSource;

	public JdbcDaoFactoryJNDI() {
		try {

			InitialContext ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("java:comp/env/jdbc/flowerShop");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public DaoConnection getConnection() {
		try {
			return new JdbcDaoConnection(dataSource.getConnection());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public PionDao createPionDao() {
		return null;
	}

	@Override
	public PionDao createPionDao(DaoConnection connection) {
		JdbcDaoConnection jdbcConnection = (JdbcDaoConnection) connection;
		Connection sqlConnection = jdbcConnection.getConnection();
		return new JdbcPionDao(sqlConnection);
	}

	@Override
	public RoseDao createRoseDao() {
		return null;
	}

	@Override
	public RoseDao createRoseDao(DaoConnection connection) {
		JdbcDaoConnection jdbcConnection = (JdbcDaoConnection) connection;
		Connection sqlConnection = jdbcConnection.getConnection();
		return new JdbcRoseDao(sqlConnection);
	}

}
