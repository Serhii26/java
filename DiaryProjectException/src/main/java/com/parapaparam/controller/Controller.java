package com.parapaparam.controller;

import com.parapaparam.model.DuplicateDataException;
import com.parapaparam.model.Model;
import com.parapaparam.model.Note;
import com.parapaparam.view.View;

import java.util.Scanner;

/**
 * Created by Serhii_Bondarchuk on 4/20/2017.
 */
public class Controller {

    Model model;
    View view;

    UtilityController utility;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        utility = new UtilityController();
    }

    public void processUser() {

        view.print(view.SALUTATION);

        process();

    }

    private void process() {

        Scanner scanner = new Scanner(System.in);

        view.print(view.CONDITION);

        while (!scanner.next().equalsIgnoreCase("q")) {

            model.setNote(new Note());

            model.getNote().setSurname(utility.inputSurnameWithScanner(view, scanner));
            model.getNote().setName(utility.inputNameWithScanner(view, scanner));
            model.getNote().setPatronymic(utility.inputPatronymicWithScanner(view, scanner));
            model.getNote().setInitials(model.createInitials(model.getNote().getSurname(), model.getNote().getName()));
            model.getNote().setNickname(utility.inputNicknameWithScanner(view, scanner));
            model.getNote().setComment(utility.inputCommentWithScanner(view, scanner));
            model.getNote().setGroup(utility.inputGroupWithScanner(view, scanner));
            model.getNote().setHomePhone(utility.inputHomephoneWithScanner(view, scanner));
            model.getNote().setPhone(utility.inputPhoneWithScanner(view, scanner));
            model.getNote().setEmail(utility.inputEmailWithScanner(view, scanner));
            model.getNote().setSkype(utility.inputSkypeWithScanner(view, scanner));
            model.getNote().setIndex(utility.inputIndexWithScanner(view, scanner));
            model.getNote().setCity(utility.inputCityWithScanner(view, scanner));
            model.getNote().setStreet(utility.inputStreetWithScanner(view, scanner));
            model.getNote().setHouseNumber(utility.inputHouseNumberWithScanner(view, scanner));
            model.getNote().setFlatNumber(utility.inputFlatNumberWithScanner(view, scanner));
            model.getNote().setFullAddress(model.createFullAddress(new String[]{model.getNote().getIndex(), model.getNote().getCity(), model.getNote().getStreet(), model.getNote().getHouseNumber(), model.getNote().getFlatNumber()}));

            while (checkNickName(scanner)) {
            }

            view.print(view.CONDITION);
        }

        view.print(model.getNoteBook().toString());
        model.exportToFile();
    }

    boolean checkNickName(Scanner sc) {
        try {
            model.getNoteBook().setRecords(model.getNote());
            view.print(view.SUCCESS);
            return false;
        } catch (DuplicateDataException ex) {
            view.print(ex.getMessage());
            view.print(view.WRONG);
            model.getNote().setNickname(utility.inputNicknameWithScanner(view, sc));
        }
        return true;
    }


}
