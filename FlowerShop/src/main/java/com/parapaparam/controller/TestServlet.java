package com.parapaparam.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.parapaparam.controller.commands.Command;
import com.parapaparam.controller.commands.GetPionById;
import com.parapaparam.controller.commands.GetPions;
import com.parapaparam.controller.commands.GetRoses;
import com.parapaparam.model.services.PionService;

//@WebServlet("/rest/*")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(TestServlet.class);

	private Map<String, Command> commands = new HashMap<>();

	PionService pionService = new PionService();

	public TestServlet() {
		super();
	}

	@Override
	public void init() {

		commands.put("GET:/pions", new GetPions());
		commands.put("GET:/pion", new GetPionById());
		commands.put("GET:/roses", new GetRoses());
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String method = request.getMethod().toUpperCase();
		String path = request.getRequestURI();
		// log.info(path);
		path = path.replaceAll(".*/rest", "").replaceAll("\\d+", "");
		String key = method + ":" + path;
		Command command = commands.getOrDefault(key, (req, resp) -> "/index.jsp");
		String viewPage = command.execute(request, response);
		request.getRequestDispatcher(viewPage).forward(request, response);
	}

}
