package com.parapaparam.Controller;

import com.parapaparam.Attempt;
import com.parapaparam.Model.Model;
import com.parapaparam.View.View;
import org.apache.log4j.Logger;

import java.util.Scanner;

/**
 * Created by Serhii_Bondarchuk on 4/18/2017.
 */
public class Controller {

    Model model;
    View view;

    // Инициализация логера
    private static final Logger log = Logger.getLogger(Controller.class);

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void processUser() {

        Scanner scanner = new Scanner(System.in);

        view.print(view.INTRO);
        model.rand();
        inputDataWithScanner(scanner);
    }

    public void inputDataWithScanner(Scanner sc) {

        Integer value;
        String wrongValue;

        view.printRules(model.getMinValue(), model.getMaxValue());

        while (sc.hasNext()) {
            if (sc.hasNextInt()) {
                value = sc.nextInt();
                log.info("Attempt " + value);
                checkValue(value);
            } else {
                wrongValue=sc.next();
                log.info(wrongValue);
                if (wrongValue.equalsIgnoreCase("q")) {
                    view.print(view.BYE);
                    break;
                } else {
                    view.printWrong(model.getMinValue(), model.getMaxValue());
                }
            }
        }
    }

    public void inputDataWithEnum(Attempt attempt) {

        Integer value;

        view.printRules(model.getMinValue(), model.getMaxValue());
        if (tryParseInt(attempt.getValue()) != null) {
            value = tryParseInt(attempt.getValue());
            checkValue(value);
        } else {
            if (attempt.getValue().equalsIgnoreCase("q")) {
                view.print(view.BYE);
            } else {
                view.printWrong(model.getMinValue(), model.getMaxValue());
            }
        }
    }

    private Integer tryParseInt(String text) {
        Integer value;
        try {
            value = Integer.valueOf(text);
            return value;
        } catch (Exception ex) {
            return null;
        }
    }

    private void checkValue(Integer val) {
        if ((val < model.getMaxValue()) && (val > model.getMinValue())) {

            model.setLog(val);

            if (val == model.getSecretValue()) {
                view.printCongrat(model.getLog(), val);
                System.exit(0);

            } else if (val < model.getSecretValue()) {

                if ((model.getMaxValue() - val) == 2) {
                    view.printCongrat(model.getLog(), model.getMinValue() + 1);
                    System.exit(0);
                }

                model.setMinValue(val);
                view.print(view.LESS);
                view.printAll(model.getMinValue(), model.getMaxValue(), model.getLog());
            } else {

                if ((val - model.getMinValue()) == 2) {
                    view.printCongrat(model.getLog(), model.getMinValue() + 1);
                    System.exit(0);
                }

                model.setMaxValue(val);
                view.print(view.MORE);
                view.printAll(model.getMinValue(), model.getMaxValue(), model.getLog());
            }

        } else {
            view.printWrong(model.getMinValue(), model.getMaxValue());
        }
    }
}
