package com.parapaparam.model.services;

import java.util.List;
import java.util.Optional;

import com.parapaparam.model.dao.DaoConnection;
import com.parapaparam.model.dao.DaoFactory;
import com.parapaparam.model.dao.PionDao;
import com.parapaparam.model.entities.Pion;

public class PionService {

	// private CityDao cityDao = .createCityDao();
	DaoFactory daoFactory = DaoFactory.getInstance();

	private static class Holder {
		static final PionService INSTANCE = new PionService();
	}

	public static PionService getInstance() {
		return Holder.INSTANCE;
	}

	// void setCityDao(CityDao cityDao) {
	// this.cityDao = cityDao;
	// }

	public List<Pion> getAllPions() {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			return pionDao.getAll();
		}
	}

	public Optional<Pion> getPionById(int pionId) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			return pionDao.getById(pionId);
		}
	}
	
	public Boolean insertPion(Pion pion) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			if (pionDao.add(pion)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}

	public Boolean deletePionById(int pionId) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			if (pionDao.delete(pionId)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
	
	public Boolean deletePion(Pion pion) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			if (pionDao.delete(pion)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
	
	public Boolean updatePion(Pion pion) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			PionDao pionDao = daoFactory.createPionDao(connection);
			if (pionDao.update(pion)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
}
