package com.parapaparam.model.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.parapaparam.model.dao.PionDao;
import com.parapaparam.model.entities.enums.Condition;
import com.parapaparam.model.entities.enums.Role;
import com.parapaparam.model.entities.Pion;

public class JdbcPionDao implements PionDao {

	private static final String SELECT_FROM_PION = "SELECT * FROM pion";
	private static final String SELECT_FROM_PION_BY_ID = "SELECT * FROM pion WHERE pion_id = ?";
	private static final String INSERT_INTO_PION = "INSERT INTO pion (length, conditions, shelf_life, expiration_date, price, role) VALUES ( ?, ?, ?, ?, ?, ? )";
	private static final String UPDATE_PION = "UPDATE pion SET length = ?, conditions = ?, shelf_life = ?, expiration_date = ?, price = ?, role = ? WHERE pion_id = ?";
	private static final String DELETE_FROM_PION_BY_ID = "DELETE FROM pion WHERE pion_id = ?";
	private static final String DELETE_FROM_PION = "DELETE FROM pion WHERE pion_id = ?";

	private static final String PION_ID = "pion_id";
	private static final String LENGTH = "length";
	private static final String CONDITITONS = "conditions";
	private static final String SHELF_LIFE = "shelf_life";
	private static final String EXPIRATIO_DATE = "expiration_date";
	private static final String PRICE = "price";
	private static final String ROLE = "role";

	private Connection connection;

	public JdbcPionDao() {
	}

	JdbcPionDao(Connection connection) {
		this.connection = connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	private Pion extractPionFromResultSet(ResultSet resultSet) throws SQLException {
		return new Pion.Builder().setId(resultSet.getInt(PION_ID))
				.setExpirationDate(resultSet.getString(EXPIRATIO_DATE))
				.setCondition(Condition.valueOf(resultSet.getString(CONDITITONS).toUpperCase()))
				.setMaxLength(resultSet.getInt(LENGTH)).setPrice(resultSet.getDouble(PRICE))
				.setShelfLife(resultSet.getInt(SHELF_LIFE))
				.setRole(Role.valueOf(resultSet.getString(ROLE).toUpperCase())).build();
	}

	@Override
	public List<Pion> getAll() {
		List<Pion> result = new ArrayList<>();
		try (Statement query = connection.createStatement();
				ResultSet resultSet = query.executeQuery(SELECT_FROM_PION)) {

			while (resultSet.next()) {
				result.add(extractPionFromResultSet(resultSet));

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
	
	@Override
	public Optional<Pion> getById(Integer id) {
		Optional<Pion> result = Optional.empty();
		try (PreparedStatement query = connection.prepareStatement(SELECT_FROM_PION_BY_ID)) {
			query.setInt(1, id);
			ResultSet resultSet = query.executeQuery();
			if (resultSet.next()) {
				result = Optional.of(extractPionFromResultSet(resultSet));
				
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	@Override
	public Boolean add(Pion pion) {
		try (PreparedStatement query = connection.prepareStatement(INSERT_INTO_PION, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, pion.getLength());
			query.setString(2, pion.getCondition().getValue());
			query.setInt(3, pion.getShelfLife());
			query.setString(4, pion.getExpirationDate());
			query.setDouble(5, pion.getPrice());
			query.setString(6, pion.getRole().getValue());
			query.executeUpdate();
			ResultSet keys = query.getGeneratedKeys();
			if (keys.next()) {
				return true;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return false;
	}

	@Override
	public Boolean delete(Pion pion) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(DELETE_FROM_PION, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, pion.getId());
			
			rowsDeleted = query.executeUpdate();
			if (rowsDeleted > 0) {
				return true;
			}
			return false;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean delete(Integer id) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(DELETE_FROM_PION_BY_ID)) {
			query.setInt(1, id);
			rowsDeleted = query.executeUpdate();

			if (rowsDeleted > 0) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Boolean update(Pion pion) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(UPDATE_PION, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, pion.getLength());
			query.setString(2, pion.getCondition().getValue());
			query.setInt(3, pion.getShelfLife());
			query.setString(4, pion.getExpirationDate());
			query.setDouble(5, pion.getPrice());
			query.setString(6, pion.getRole().getValue());
			query.setInt(7, pion.getId());
			
			rowsDeleted = query.executeUpdate();
			if (rowsDeleted > 0) {
				return true;
			}
			return false;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
