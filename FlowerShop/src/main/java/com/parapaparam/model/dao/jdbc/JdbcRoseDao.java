package com.parapaparam.model.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.parapaparam.model.dao.RoseDao;
import com.parapaparam.model.entities.enums.Condition;
import com.parapaparam.model.entities.enums.Role;
import com.parapaparam.model.entities.Rose;

public class JdbcRoseDao implements RoseDao {

	private static final String SELECT_FROM_ROSE = "SELECT * FROM Rose";
	private static final String SELECT_FROM_ROSE_BY_ID = "SELECT * FROM Rose WHERE rose_id = ?";
	private static final String INSERT_INTO_ROSE = "INSERT INTO Rose (length, conditions, shelf_life, expiration_date, price, role) VALUES ( ?, ?, ?, ?, ?, ? )";
	private static final String UPDATE_ROSE = "UPDATE Rose SET length = ?, conditions = ?, shelf_life = ?, expiration_date = ?, price = ?, role = ? WHERE rose_id = ?";
	private static final String DELETE_FROM_ROSE_BY_ID = "DELETE FROM Rose WHERE rose_id = ?";
	private static final String DELETE_FROM_ROSE = "DELETE FROM Rose WHERE rose_id = ?";

	private static final String ROSE_ID = "rose_id";
	private static final String LENGTH = "length";
	private static final String CONDITITONS = "conditions";
	private static final String SHELF_LIFE = "shelf_life";
	private static final String EXPIRATIO_DATE = "expiration_date";
	private static final String PRICE = "price";
	private static final String ROLE = "role";

	private Connection connection;

	public JdbcRoseDao() {
	}

	JdbcRoseDao(Connection connection) {
		this.connection = connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	private Rose extractRoseFromResultSet(ResultSet resultSet) throws SQLException {
		return new Rose.Builder().setId(resultSet.getInt(ROSE_ID))
				.setExpirationDate(resultSet.getString(EXPIRATIO_DATE))
				.setCondition(Condition.valueOf(resultSet.getString(CONDITITONS).toUpperCase()))
				.setMaxLength(resultSet.getInt(LENGTH)).setPrice(resultSet.getDouble(PRICE))
				.setShelfLife(resultSet.getInt(SHELF_LIFE))
				.setRole(Role.valueOf(resultSet.getString(ROLE).toUpperCase())).build();
	}

	@Override
	public List<Rose> getAll() {
		List<Rose> result = new ArrayList<>();
		try (Statement query = connection.createStatement();
				ResultSet resultSet = query.executeQuery(SELECT_FROM_ROSE)) {

			while (resultSet.next()) {
				result.add(extractRoseFromResultSet(resultSet));

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
	
	@Override
	public Optional<Rose> getById(Integer id) {
		Optional<Rose> result = Optional.empty();
		try (PreparedStatement query = connection.prepareStatement(SELECT_FROM_ROSE_BY_ID)) {
			query.setInt(1, id);
			ResultSet resultSet = query.executeQuery();
			if (resultSet.next()) {
				result = Optional.of(extractRoseFromResultSet(resultSet));
				
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return result;
	}

	@Override
	public Boolean add(Rose rose) {
		try (PreparedStatement query = connection.prepareStatement(INSERT_INTO_ROSE, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, rose.getLength());
			query.setString(2, rose.getCondition().getValue());
			query.setInt(3, rose.getShelfLife());
			query.setString(4, rose.getExpirationDate());
			query.setDouble(5, rose.getPrice());
			query.setString(6, rose.getRole().getValue());
			query.executeUpdate();
			ResultSet keys = query.getGeneratedKeys();
			if (keys.next()) {
				return true;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return false;
	}

	@Override
	public Boolean delete(Rose rose) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(DELETE_FROM_ROSE, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, rose.getId());
			
			rowsDeleted = query.executeUpdate();
			if (rowsDeleted > 0) {
				return true;
			}
			return false;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Boolean delete(Integer id) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(DELETE_FROM_ROSE_BY_ID)) {
			query.setInt(1, id);
			rowsDeleted = query.executeUpdate();

			if (rowsDeleted > 0) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Boolean update(Rose rose) {
		int rowsDeleted;
		try (PreparedStatement query = connection.prepareStatement(UPDATE_ROSE, Statement.RETURN_GENERATED_KEYS)) {
			query.setInt(1, rose.getLength());
			query.setString(2, rose.getCondition().getValue());
			query.setInt(3, rose.getShelfLife());
			query.setString(4, rose.getExpirationDate());
			query.setDouble(5, rose.getPrice());
			query.setString(6, rose.getRole().getValue());
			query.setInt(7, rose.getId());
			
			rowsDeleted = query.executeUpdate();
			if (rowsDeleted > 0) {
				return true;
			}
			return false;
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
