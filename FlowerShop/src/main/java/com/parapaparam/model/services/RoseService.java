package com.parapaparam.model.services;

import java.util.List;
import java.util.Optional;

import com.parapaparam.model.dao.DaoConnection;
import com.parapaparam.model.dao.DaoFactory;
import com.parapaparam.model.dao.RoseDao;
import com.parapaparam.model.entities.Rose;

public class RoseService {

	DaoFactory daoFactory = DaoFactory.getInstance();

	private static class Holder {
		static final RoseService INSTANCE = new RoseService();
	}

	public static RoseService getInstance() {
		return Holder.INSTANCE;
	}

	public List<Rose> getAllRoses() {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao roseDao = daoFactory.createRoseDao(connection);
			return roseDao.getAll();
		}
	}

	public Optional<Rose> getRoseById(int roseId) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao RoseDao = daoFactory.createRoseDao(connection);
			return RoseDao.getById(roseId);
		}
	}
	
	public Boolean insertRose(Rose rose) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao RoseDao = daoFactory.createRoseDao(connection);
			if (RoseDao.add(rose)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}

	public Boolean deleteRoseById(int roseId) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao RoseDao = daoFactory.createRoseDao(connection);
			if (RoseDao.delete(roseId)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
	
	public Boolean deleteRose(Rose rose) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao RoseDao = daoFactory.createRoseDao(connection);
			if (RoseDao.delete(rose)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
	
	public Boolean updateRose(Rose rose) {
		try (DaoConnection connection = daoFactory.getConnection()) {
			connection.begin();
			RoseDao RoseDao = daoFactory.createRoseDao(connection);
			if (RoseDao.update(rose)) {
				connection.commit();
				return true;
			}
			return false;
		}
	}
}
