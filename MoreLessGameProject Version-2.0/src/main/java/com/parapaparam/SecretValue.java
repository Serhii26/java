package com.parapaparam;

/**
 * Created by Serhii_Bondarchuk on 4/19/2017.
 */
public enum SecretValue {
    FIRST (1),
    SECOND (2),
    THIRD (3),
    FOURTH (4),
    FIFTH (5),
    SIXTH (6);

    private final Integer value;

    SecretValue(Integer mass) {
        this.value = mass;
    }
    public Integer getValue() { return value; }
}
