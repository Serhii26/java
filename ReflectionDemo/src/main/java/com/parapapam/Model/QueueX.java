package com.parapapam.Model;

import com.parapapam.Model.StackX;
import com.parapapam.Resources.CustomAnnotation;

public class QueueX extends StackX {
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;
    private int nItems;

    //--------------------------------------------------------------
    public QueueX(int s) // Конструктор
    {
        super(s);
        maxSize = s;
        queArray = new long[maxSize];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public int getRear() {
        return rear;
    }

    public void setRear(int rear) {
        this.rear = rear;
    }

    public int getnItems() {
        return nItems;
    }

    public void setnItems(int nItems) {
        this.nItems = nItems;
    }

    //--------------------------------------------------------------
    @Override
    public void push(long j) // Вставка элемента в конец очереди
    {
        if (rear == maxSize - 1) { // Циклический перенос
            rear = -1;
        }
        queArray[++rear] = j; // Увеличение rear и вставка
        nItems++; // Увеличение количества элементов
    }

    //--------------------------------------------------------------
    @Override
    public long pop() // Извлечение элемента в начале очереди
    {
        long temp = queArray[front++]; // Выборка и увеличение front
        if (front == maxSize) // Циклический перенос
            front = 0;
        nItems--; // Уменьшение количества элементов
        return temp;
    }

    //--------------------------------------------------------------
    @Override
    @CustomAnnotation
    public long peek() // Чтение элемента в начале очереди
    {
        return queArray[front];
    }

    //--------------------------------------------------------------
    @Override
    public boolean isEmpty() // true, если очередь пуста
    {
        return (nItems == 0);
    }

    //--------------------------------------------------------------
    @Override
//    @CustomAnnotation
    public boolean isFull() // true, если очередь заполнена
    {
        return (nItems == maxSize);
    }

    //--------------------------------------------------------------
    public int size() // Количество элементов в очереди
    {
        return nItems;
    }

    //--------------------------------------------------------------

    @Override
    public String toString() {
        String s = "";
        for (long item : queArray) {
            s += item + " ";
        }
        return s;
    }
} // Конец класса Queue

