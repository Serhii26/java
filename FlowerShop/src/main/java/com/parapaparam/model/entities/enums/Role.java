package com.parapaparam.model.entities.enums;

public enum Role {

	SECONDARY("Secondary"), PRIMARY("Primary");

	private final String value;

	Role(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
