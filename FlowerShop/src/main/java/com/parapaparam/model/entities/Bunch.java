package com.parapaparam.model.entities;

import java.util.ArrayList;

import com.parapaparam.model.entities.enums.Accessory;

public class Bunch {
	private ArrayList<Flower> flowers;
	private ArrayList<Accessory> accessories;
	private Double cost = 0.0;

	// Look to Pattern Builder !!!!!Look to Pattern Builder!!!!!!!!!!!Look to
	// Pattern Builder

	public Bunch() {
		flowers = new ArrayList<Flower>();
		accessories = new ArrayList<Accessory>();
	}

//	public Bunch(ArrayList<Flower> flowers) {
//		this.flowers = flowers;
//		accessories = new ArrayList<Accessory>();
//	}

//	public Bunch(ArrayList<Flower> flowers, ArrayList<Accessory> accessories) {
//		this.flowers = flowers;
//		this.accessories = accessories;
//	}

	public Bunch(Class flowerClass, Integer quantity) {
		for (int i = 0; i < quantity; i++) {
//			flowers.add(flower);
		}
		accessories = new ArrayList<Accessory>();
	}

	public Bunch(Flower flower, Integer quantity, ArrayList<Accessory> accessories) {
		for (int i = 0; i < quantity; i++) {
			flowers.add(flower);
		}
		this.accessories = accessories;
	}

	public ArrayList<Flower> getFlowers() {
		return flowers;
	}

	public void setFlowers(ArrayList<Flower> flowers) {
		this.flowers = flowers;
	}

	public ArrayList<Accessory> getAccessories() {
		return accessories;
	}

	public void setAccessories(ArrayList<Accessory> accessories) {
		this.accessories = accessories;
	}
	
	public Double getCost() {
		calcCost();
		return cost;
	}

	public void addFlower(Flower flower) {
		flowers.add(flower);
	}

	public void addFlowers(ArrayList<Flower> flowers) {
		this.flowers.addAll(flowers);
	}

	public void addFlowers(Flower flower, Integer quantity) {
		for (int i = 0; i < quantity; i++) {
			flowers.add(flower);
		}
	}

	public void addAccessory(Accessory accessory) {
		accessories.add(accessory);
	}

	public void addAccessories(ArrayList<Accessory> accessories) {
		this.accessories.addAll(accessories);
	}

	
	private void calcCost() {
		for (Flower flower : flowers) {
			cost += flower.getPrice();
		}
		for (Accessory accessory: accessories) {
			cost += accessory.getPrice();
		}
	}
}
