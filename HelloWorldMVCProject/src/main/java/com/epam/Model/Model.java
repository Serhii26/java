package com.epam.Model;

/**
 * Created by Seroga on 12.04.2017.
 */
public class Model {

    private String message1;
    private String message2;
    private String message;

    public String getMessage1() {
        return message1;
    }

    public void setMessage1(String message1) {
        this.message1 = message1;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage() {
        this.message = message1 + " " + message2 + "!";
    }
}
