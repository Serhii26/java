package com.parapaparam.model;

import com.sun.org.apache.xml.internal.serialize.LineSeparator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Serhii_Bondarchuk on 4/21/2017.
 */
public class Note {

    private String surname;
    private String name;
    private String initials;
    private String patronymic;
    private String nickname;
    private String comment;
    private String group;
    private String homePhone;
    private String phone;
    private String email;
    private String skype;
    private String index;
    private String city;
    private String street;
    private String houseNumber;
    private String flatNumber;
    //    private ArrayList<Object> address;
//    private String[] address;
    private String fullAddress;
    private String entryDate;
    private String lastModifiedDate;

    public Note() {
        entryDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
        lastModifiedDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(new Date());
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

//    public String[] getAddress() {
//        return address;
//    }
//
//    public void setAddress(String[] address) {
//        this.address = address;
//    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public String toString() {
        StringBuffer record = new StringBuffer();
        record.append("***********Abonent**************");
        record.append(LineSeparator.Windows);
        record.append("Surname: " + surname);
        record.append(System.getProperty("line.separator"));
        record.append("Name: " + name);
        record.append(LineSeparator.Windows);
        record.append("Initials: " + initials);
        record.append(LineSeparator.Windows);
        record.append("Patronymic: " + patronymic);
        record.append(LineSeparator.Windows);
        record.append("NickName: " + nickname);
        record.append(LineSeparator.Windows);
        record.append("Comment: " + comment);
        record.append(LineSeparator.Windows);
        record.append("Group: " + group);
        record.append(LineSeparator.Windows);
        record.append("Home Phone: " + homePhone);
        record.append(LineSeparator.Windows);
        record.append("Phone: " + phone);
        record.append(LineSeparator.Windows);
        record.append("E-mail: " + email);
        record.append(LineSeparator.Windows);
        record.append("Skype: " + skype);
        record.append(LineSeparator.Windows);
        record.append("Index: " + index);
        record.append(LineSeparator.Windows);
        record.append("City: " + city);
        record.append(LineSeparator.Windows);
        record.append("Street: " + street);
        record.append(LineSeparator.Windows);
        record.append("House Number: " + houseNumber);
        record.append(LineSeparator.Windows);
        record.append("Flat Number: " + flatNumber);
        record.append(LineSeparator.Windows);
        record.append("Full Address: " + fullAddress);
        record.append(LineSeparator.Windows);
        record.append("Entry date: " + entryDate);
        record.append(LineSeparator.Windows);
        record.append("Last Modified date: " + lastModifiedDate);
        record.append(LineSeparator.Windows);
        return record.toString();
    }
}
