package com.parapaparam.view;

import com.parapaparam.model.Note;

/**
 * Created by Serhii_Bondarchuk on 4/20/2017.
 */
public class View {

    public static final String SALUTATION = "Salut Welcome!";

    public static final String SURNAME = "Please, input your surname in format [Doe]";
    public static final String NAME = "Please, input your name in format [John]";
    public static final String PATRONYMIC = "Please, input your patronymic in format [Ivanovich]";
    public static final String NICKNAME = "Please, input your nickname in format [Usver_24]";
    public static final String COMMENT = "Please, input your comment";
    public static final String GROUP = "Please, input your Group: AT, SFT, FRONTEND, DOTNET, JAVA!";
    public static final String HOMEPHONE = "Please, input your homephone in format [(094)345-34-34]";
    public static final String PHONE = "Please, input your phone in format [(094)345-34-34]";
    public static final String EMAIL = "Please, input your email in format [text@hfsd.rt]";
    public static final String SKYPE = "Please, input your skype in format [skype23]";
    public static final String INDEX = "Please, input your index in format [31100]";
    public static final String CITY = "Please, input your city in format [Kyiv]";
    public static final String STREET = "Please, input your street in format [Khreschatyk]";
    public static final String HOUSENUMBER = "Please, input your house number in format [36]";
    public static final String FLATNUMBER = "Please, input your flat number in format [230]";

    public static final String CONDITION = "To quit press \'q\'! To continue press any key...";
    public static final String WRONG = "Wrong input!";
    public static final String CORRECT = "Correct!!!";
    public static final String SUCCESS = "Data was success saved!";

    public void print(String message){
        System.out.println(message);
    }

    public void print(Note record){
        System.out.println(record);
    }

}
