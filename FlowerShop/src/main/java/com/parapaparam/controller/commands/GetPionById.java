package com.parapaparam.controller.commands;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.parapaparam.model.entities.Pion;
import com.parapaparam.model.services.PionService;

public class GetPionById implements Command {

	PionService pionService = new PionService();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = request.getQueryString();
		int pionId = Integer.parseInt(path.replaceAll("\\D+", ""));
		Optional<Pion> pion = pionService.getPionById(pionId);
		if (pion.isPresent()) {
			request.setAttribute("pion", pion.get());
			return "/Pion.jsp";
		} else
			return "/index.jsp";
	}

}
