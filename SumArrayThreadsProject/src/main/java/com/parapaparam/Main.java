package com.parapaparam;

/**
 * Hello world!
 */
public class Main {

    private volatile int[] array;
    private volatile int sum;

    public Main() {
        array = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = 1;
        }
    }

    public Main(int[] array) {
        this.array = array;
        sum = 0;
    }

    public Main(int size) {
        array = new int[size];
    }

    public static void main(String[] args) {
        System.out.println("Sum of Array: " + new Main().getSum());
    }

    private int getSum() {
        FirstThread f = new FirstThread();
        SecondThread s = new SecondThread();

        f.run();
        s.run();
        return sum;
    }

    class FirstThread extends Thread {
        public void run() {
            for (int i = 0; i < array.length / 2; i++) {
                sum += array[i];
            }
        }
    }

    class SecondThread extends Thread {
        public void run() {
            for (int i = array.length / 2; i < array.length; i++) {
                sum += array[i];
            }
        }
    }
}



