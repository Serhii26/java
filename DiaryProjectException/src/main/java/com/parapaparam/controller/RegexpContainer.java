package com.parapaparam.controller;

/**
 * Created by Serhii_Bondarchuk on 4/20/2017.
 */
interface RegexpContainer {

    String surname = "^[a-zA-Z]{2,40}$";
    String name = "^[a-zA-Z]{2,30}$";
    String patronymic = "^[a-zA-Z]{2,40}$";
    String nickname = "^[\\w]{1,40}$";
    String comment = "^[\\w]{0,100}$";
    String homePhone = "^\\(([0-9]){3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}$";
    String phone = "^\\(([0-9]){3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2}$";
    String phone2 = "^(\\(([0-9]){3}\\)[0-9]{3}-[0-9]{2}-[0-9]{2})?$";
    String email = "^[\\w]{3,20}@[a-zA-Z]{2,5}\\.[a-zA-Z]{2,4}(\\.[a-zA-Z]{2,4})?$";
    String skype = "^[\\w]{3,20}?$";
    String index = "^[\\d]{5}$";
    String city = "^[\\w]{2,40}$";
    String street = "^[\\w]{3,30}$";
    String houseNumber = "^[\\d]{1,5}(\\/[\\d]{1,3})?$";
    String flatNumber = "^[\\d]{1,4}$";

}
