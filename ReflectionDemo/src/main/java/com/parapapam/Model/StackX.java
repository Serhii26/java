package com.parapapam.Model;

import com.parapapam.Resources.CustomAnnotation;

import java.util.Arrays;

/**
 * Created by Serhii_Bondarchuk on 5/5/2017.
 */
public class StackX {
    private int maxSize; // Размер массива
    private long[] stackArray;
    private int top; // Вершина стека



    //--------------------------------------------------------------
    public StackX(int s) // Конструктор
    {
        maxSize = s; // Определение размера стека
        stackArray = new long[maxSize]; // Создание массива
        top = -1; // Пока нет ни одного элемента
    }

    //--------------------------------------------------------------
    @CustomAnnotation
    public void push(long j) // Размещение элемента на вершине стека
    {
        stackArray[++top] = j; // Увеличение top, вставка элемента
    }

    //--------------------------------------------------------------
    @CustomAnnotation
    public long pop() // Извлечение элемента с вершины стека
    {
        return stackArray[top--]; // Извлечение элемента, уменьшение top
    }

    //--------------------------------------------------------------
    public long peek() // Чтение элемента с вершины стека
    {
        return stackArray[top];
    }

    //--------------------------------------------------------------
    @CustomAnnotation
    public boolean isEmpty() // True, если стек пуст
    {
        return (top == -1);
    }

    //--------------------------------------------------------------
    public boolean isFull() // True, если стек полон
    {
        return (top == maxSize - 1);
    }

    @Override
    public String toString() {
        return "StackX{" +
                "stackArray=" + Arrays.toString(stackArray) +
                '}';
    }

    //--------------------------------------------------------------
} // Конец класса StackX
