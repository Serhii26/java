package com.paramparam.Model;

import java.util.Arrays;

/**
 * Created by Serhii_Bondarchuk on 4/13/2017.
 */
public class Model {

    private Integer[] inputArray;
    private Integer inputValue;

    private Integer[] resultArray;
    private Integer resultValue;

    public Integer[] getInputArray() {
        return inputArray;
    }

    public void setInputArray(Integer[] inputArray) {
        this.inputArray = inputArray;
    }

    public void setInputValue(Integer inputValue) {
        this.inputValue = inputValue;
    }

    public Integer[] getResultArray() {
        return resultArray;
    }

    public Integer getResultValue() {
        return resultValue;
    }

    // 001

    /**
     * Returns Sum of all elements of array.
     *
     * @return Sum of all elements of array
     */
    public Integer getSum() {
        resultValue = 0;
        for (int i = 0; i < inputArray.length; i++) {
            resultValue += inputArray[i];
        }
        return resultValue;
    }

    // 002

    /**
     * Returns Max element of array.
     *
     * @return Max element of array
     */
    public Integer[] getMax() {
        resultArray = new Integer[2];
        resultArray[0] = inputArray[0];
        resultArray[1] = 0;
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > resultArray[0]) {
                resultArray[0] = inputArray[i];
                resultArray[1] = i;
            }
        }
        return resultArray;
    }

    // 004

    /**
     * Returns Average of array elements.
     *
     * @return Average of array elements
     */
    public Integer getAverage() {
        resultValue = 0;
        for (int i = 0; i < inputArray.length; i++) {
            resultValue += inputArray[i];
        }
        return (resultValue / inputArray.length);
    }

    // 008

    /**
     * Returns Array multiply by number.
     *
     * @return Array multiply by number
     */
    public Integer[] getArrayMultByNumber() {
        resultArray = new Integer[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            resultArray[i] = inputArray[i] * inputValue;
        }
        return resultArray;
    }

    // 018

    /**
     * Returns Array with cyclic shift by number.
     *
     * @return Array with cyclic shift by number
     */
    public Integer[] getCyclicShift() {
        resultArray = new Integer[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            if ((inputArray.length + inputValue > 2 * inputArray.length))
                inputValue = inputValue % inputArray.length;
            if ((i + inputValue) > inputArray.length - 1)
                resultArray[(i + inputValue) - inputArray.length] = inputArray[i];
            else
                resultArray[i + inputValue] = inputArray[i];
        }
        return resultArray;
    }

    // 025

    /**
     * Returns Array with exchanges first and last elements.
     *
     * @return Array with exchanges first and last elements
     */
    public Integer[] getExchangeFirstAndLast() {
        resultArray = new Integer[inputArray.length];
        System.arraycopy(inputArray, 0, resultArray, 0, inputArray.length);
        int tmp;
        int bar = resultArray.length;
        for (int i = 0; i < resultArray.length; i++) {
            if (resultArray[i] > 0) {
                for (int j = bar - 1; j >= i; j--) {
                    if (resultArray[j] > 0) {
                        bar = j;
                        tmp = resultArray[i];
                        resultArray[i] = resultArray[j];
                        resultArray[j] = tmp;
                        break;
                    }
                }
            }
        }
        return resultArray;
    }
}
